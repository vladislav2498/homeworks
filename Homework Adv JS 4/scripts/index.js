"use strict"

const url = 'https://ajax.test-danit.com/api/swapi/films';

try {
    fetch(url)
        .then(response => response.json())
        .then(res => {
            res.sort((a, b) => {
                return a.episodeId - b.episodeId;
            });
            res.forEach(({name, episodeId, openingCrawl, characters}) => {
                document.querySelector('.container').insertAdjacentHTML('beforeend', `
    <div class="card">
        <h3 class="card__title">${name}</h3>
        <h4 class="card__episode">episode ${episodeId}</h4>
        <p class="card__description">${openingCrawl}</p>
        <ul class="card__num card__num_${episodeId}"><div class="card__placeholder">${characters}</div></ul>
    </div>`
                );
                const charactersArr = characters.map((el) =>
                    fetch(el).then((response) => response.json())
                );
                Promise.allSettled(charactersArr).then((result) => {
                    document.querySelector(`.card__num_${episodeId}`).innerHTML = '';
                    result.forEach((elem) => {
                        const {
                            value: {name},
                        } = elem;
                        if (elem.status === "fulfilled") {
                            document.querySelector(`.card__num_${episodeId}`).insertAdjacentHTML('beforeend', `<li>${name}</li>`)
                        }
                    });
                });
            })
        });
} catch(err) {
    document.body.innerHTML = `<h1>some Error: ${err}</h1>`
}

"use strict";

/* Теоретический вопрос 1

    В JavaScript есть 8 основных типов данных.

        number для любых чисел: целочисленных или чисел с плавающей точкой; целочисленные значения ограничены диапазоном ±(253-1).
        bigint для целых чисел произвольной длины.
        string для строк. Строка может содержать ноль или больше символов, нет отдельного символьного типа.
        boolean для true/false.
        null для неизвестных значений – отдельный тип, имеющий одно значение null.
        undefined для неприсвоенных значений – отдельный тип, имеющий одно значение undefined.
        object для более сложных структур данных.
        symbol для уникальных идентификаторов.



    Теоретический вопрос 2

        Разница между ==(равно) и ===(строгое равно) в том, что в первом случае сравниваются данные по значению, а во втором еще и по типу данных.



    Теоретический вопрос 3

        Оператор — это специальный символ или выражение для проверки, изменения или сложения величин.

        В JavaScript есть следующие типы операторов.

            Операторы присваивания
            Операторы сравнения
            Арифметические операторы
            Битовые (поразрядные) операторы
            Логические операторы
            Строковые операторы
            Условный (тернарный) оператор
            Оператор запятая
            Унарные операторы
            Операторы отношения
 */

    let userName = prompt("What is your name?");
    while (userName === '' || userName === ' ') {
        userName = prompt("What is your name?");
    }

    let userAge = +prompt("How old are you?");
    while (isNaN(userAge) || userAge === '' || userAge === ' ' || userAge === 0) {
        userAge = +prompt("How old are you?");
    }

    if (userAge < 18) {
        alert("You are not allowed to visit this website.");
    } else if (userAge > 22) {
        alert("Welcome " + userName);
    } else if (confirm("Are you sure you want to continue?")) {
        alert("Welcome " + userName);
    } else {
        alert("You are not allowed to visit this website.");
    }









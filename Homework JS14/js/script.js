"use strict";

const btnColor = document.querySelector('.feather')

btnColor.addEventListener('click', () => {
    light()

})



const light = () => {
    const name = localStorage.getItem("color");
    if (!name) {
        document.querySelector('body').classList.add('dark')
        localStorage.setItem("color", "dark")
    } else {
        document.querySelector('.dark').classList.remove('dark')
        localStorage.removeItem("color");
    }
}


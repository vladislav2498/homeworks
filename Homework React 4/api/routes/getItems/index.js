const {items} = require('../../data');
const {sendGoodResponse} = require("../utils");

const getItems = (app) => {
    app.get('/items', (req, res) => {
        setTimeout(() => {
            sendGoodResponse(res, items);
        }, 3000)
    })
}
module.exports = getItems;
import styles from './App.module.scss';
import Header from "./components/Header";
import {BrowserRouter} from "react-router-dom";
import AppRoutes from "./AppRoutes";

const App = () => {

    return (
        <BrowserRouter>
            <div className={styles.App}>
                <Header/>
                <AppRoutes/>
            </div>
        </BrowserRouter>
    );
}

export default App;




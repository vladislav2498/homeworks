import React, {useEffect, useState} from 'react';
import FavoriteItem from "../FavoriteItem/FavoriteItem";

const FavoriteContainer = () => {

    const [favorite, setFavorite] = useState([])

    const deleteFavoriteItem = (code) => {
        setFavorite((current) => {
            const favorite = [...current]

            const index = favorite.findIndex(el => el.code === code)


            if (index !== -1) {
                favorite.splice(index, 1);
            }

            localStorage.setItem('favorites', JSON.stringify(favorite))

            return favorite
        })
    }

    useEffect(() => {

        const favorite = localStorage.getItem('favorites')

        if (favorite) {
            setFavorite(JSON.parse(favorite))
        }
    }, [])

    return (
        <ul>
            <ul>
                {favorite.map(({name, url, count, id, code}) => {
                    return <FavoriteItem
                        deleteFavoriteItem={deleteFavoriteItem}
                        key={code}
                        name={name} url={url} count={count} id={id} code={code}/>
                })}
            </ul>
        </ul>
    );
}

export default FavoriteContainer;
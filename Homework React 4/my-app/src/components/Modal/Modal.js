import styles from './Modal.module.scss';
import Button from '../Button'

const Modal = ({isOpen, toggleModal, deleteCartItem, modalProps}) => {

    if (!isOpen) {
        return null;
    }

    return (
        <div className={styles.root}>
            <div onClick={() => toggleModal(false)} className={styles.background}/>
            <div className={styles.content}>
                <div className={styles.closeWrapper}>
                    <Button onClick={() => toggleModal(false)} className={styles.btn} color="red">X</Button>
                </div>
                <h2>Do you want to delete {modalProps.name} from cart?</h2>
                <div className={styles.buttonContainer}>
                    <Button onClick={() => {
                        deleteCartItem(modalProps.code);
                        toggleModal(false)
                    }}>Yes</Button>
                    <Button onClick={() => toggleModal(false)} color="red">No</Button>
                </div>
            </div>
        </div>
    );
}

export default Modal;
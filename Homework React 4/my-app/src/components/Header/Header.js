import {PureComponent} from 'react';
import styles from './Header.module.scss';
import HeaderUser from "../HeaderUser";
import cartIcon from '../../svg/cart-outline.svg'
import Navigation from "../Navigation/Navigation";

const Header = () => {

        return (
            <header className={styles.root}>
                <HeaderUser/>
                {/* Centered navigation */}
                <Navigation/>

                {/* Right container */}
                <ul>
                    <li>
                        <a href=""><img src={cartIcon} alt='Cart'/></a>
                    </li>
                </ul>
            </header>
        );
}

export default Header;

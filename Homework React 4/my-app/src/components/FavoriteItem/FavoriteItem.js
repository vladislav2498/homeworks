import React from 'react';
import styles from "../CardItem/CardItem.module.scss";
import heartSvg from "../../svg/heart.svg";

const FavoriteItem = ({url, name, code, color, price, deleteFavoriteItem }) => {

    return (
        <div className={styles.card}>
            <button onClick={() => {
                deleteFavoriteItem(code);

            }} type="button" className={styles.likeButton}>
                <img src={heartSvg} alt='favorite'/>
            </button>
            <span className={styles.title}>{name}</span>
            <img className={styles.itemAvatar} src={url}
                 alt={name}/>
            <span className={styles.description}>code: {code}</span>
            <span className={styles.description}>color: {color}</span>
            <span className={styles.description}>price: {price}$</span>
        </div>
    );

}

export default FavoriteItem;
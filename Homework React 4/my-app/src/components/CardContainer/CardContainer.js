import React from 'react';
import CardItem from "../CardItem";
import styles from './CardContainer.module.scss';

const CardContainer = ({addToCart, cards, handleClick, addToFavorites}) => {


    return (
        <ul className={styles.list}>
            {cards.map(({name, url, color, price, code, isFavorite}) => (
                <li key={code}>
                    <CardItem
                        addToFavorites={addToFavorites}
                        handleClick={handleClick}
                        addToCart={addToCart}
                        name={name}
                        url={url}
                        isFavorite={isFavorite}
                        color={color}
                        price={price}
                        code={code}/>
                </li>
            ))}
        </ul>
    )
}

export default CardContainer;
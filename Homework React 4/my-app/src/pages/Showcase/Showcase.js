import React, {useEffect, useState} from 'react';
import styles from "../../App.module.scss";
import Preloader from "../../components/Preloader";
import CardContainer from "../../components/CardContainer/CardContainer";
import AddModal from "../../components/AddModal/AddModal";


const Showcase = () => {

    const [cards, setCards] = useState([])
    const [isCardsLoading, setIsCardsLoading] = useState(true)
    const [carts, setCarts] = useState([])
    const [favorites, setFavorites] = useState([])
    const [addModalCards, setAddModalCards] = useState({})
    const [modalActive, setModalActive] = useState(false)


    const addToCart = (card) => {
        setCarts((current) => {
            const carts = [...current]

            const index = carts.findIndex(el => el.code === card.code)

            if (index === -1) {
                carts.push({...card, count: 1})
            } else {
                carts[index].count += 1
            }

            localStorage.setItem('carts', JSON.stringify(carts))

            return carts
        })
    }

    const addToFavorites = (favoriteCard) => {
        setFavorites((current) => {
            const favorites = [...current]

            const index = favorites.findIndex(el => el.code === favoriteCard.code)

            if (index === -1) {
                favorites.push({...favoriteCard, count: 1})
            } else {
                favorites.splice(index, 1)
            }

            localStorage.setItem('favorites', JSON.stringify(favorites))

            return favorites
        })
    }

    const handleClick = (name, url, code) => {
        setAddModalCards(() => {
            const newObj = {
                name,
                url,
                code
            }
            console.log(newObj)
            return (newObj)
        })
        setModalActive(() => (!modalActive))
    }


    useEffect(() => {
        const getData = async () => {
            const data = await fetch('./data.json').then(res => res.json());



            setCards(data)
            setIsCardsLoading(false)
        }
        getData()
    }, [])


    return (
        <section className={styles.leftContainer}>
            <h1>Каталог</h1>
            {isCardsLoading && <Preloader size={90}/>}
            {!isCardsLoading &&
                <CardContainer addToFavorites={addToFavorites} handleClick={handleClick} cards={cards}/>}
            {modalActive &&
                <AddModal
                    addToCart={addToCart}
                    closeButton={true}
                    clickHandler={handleClick}
                    addModalCards={addModalCards}
                />
            }
        </section>
    );
};

export default Showcase;
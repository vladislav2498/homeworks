import React, {useState, useEffect} from 'react';
import styles from './Favorite.module.scss'
import FavoriteContainer from "../../components/FavoriteContainer/FavoriteContainer";


const Favorite = () => {


    return (
        <section className={styles.favoriteContainer}>
            <h2>Favorite</h2>
            <FavoriteContainer  />
        </section>
    );
};

export default Favorite;
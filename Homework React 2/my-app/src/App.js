import {Component} from "react";
import styles from './App.module.scss';
import Header from "./components/Header";
import Preloader from "./components/Preloader";
import CardContainer from "./components/CardContainer/CardContainer";
import CartContainer from "./components/CartContainer/CartContainer";
import Modal from "./components/Modal/Modal";
import AddModal from "./components/AddModal/AddModal";
// import FavoriteContainer from "./components/FavoriteContainer/FavoriteContainer";


class App extends Component {

    state = {
        cards: [],
        isCardsLoading: true,
        carts: [],
        isOpenModal: false,
        modalProps: {},
        modalActive: false,
        favorites: [],
        addModalCards: {},
    }

    addToCart = (card) => {
        console.log(card)
        this.setState((current) => {
            const carts = [...current.carts]

            const index = carts.findIndex(el => el.code === card.code)

            if (index === -1) {
                carts.push({...card, count: 1})
            } else {
                carts[index].count += 1
            }

            localStorage.setItem('carts', JSON.stringify(carts))

            return {carts}
        })
    }

    addToFavorites = (favoriteCard) => {
        this.setState((current) => {
            const favorites = [...current.favorites]

            const index2 = favorites.findIndex(el => el.code === favoriteCard.code)

            if (index2 === -1) {
                favorites.push({...favoriteCard, count: 1})
            } else {
                favorites.splice(index2, 1)
            }

            localStorage.setItem('favorites', JSON.stringify(favorites))

            return {favorites}
        })
    }

    deleteCartItem = (code) => {
        this.setState((current) => {
            const carts = [...current.carts]

            const index = carts.findIndex(el => el.code === code)

            if (index !== -1) {
                carts.splice(index, 1);
            }

            localStorage.setItem('carts', JSON.stringify(carts))

            return {carts}
        })
    }

    incrementCartItem = (code) => {
        this.setState((current) => {
            const carts = [...current.carts]

            const index = carts.findIndex(el => el.code === code)

            if (index !== -1) {
                carts[index].count += 1
            }

            localStorage.setItem('carts', JSON.stringify(carts))

            return {carts}
        })
    }

    decrementCartItem = (code) => {
        this.setState((current) => {
            const carts = [...current.carts]

            const index = carts.findIndex(el => el.code === code)

            if (index !== -1 && carts[index].count > 1) {
                carts[index].count -= 1
            }

            localStorage.setItem('carts', JSON.stringify(carts))

            return {carts}
        })
    }


    toggleModal = (value) => {
        this.setState({isOpenModal: value})
    }

    setModalProps = (value) => {
        this.setState({modalProps: value})
    }

    handleClick = (name, url, code) => {
        this.setState(() => {
            const newObj = {
                name,
                url,
                code
            }
            console.log(newObj)
            return ({addModalCards:  newObj})
        })
        this.setState((prev) => ({modalActive: !prev.modalActive}))
    }

    async componentDidMount() {
        const data = await fetch('./data.json').then(res => res.json());

        const carts = localStorage.getItem('carts')

        if (carts) {
            this.setState({carts: JSON.parse(carts)})
        }

        const favorites = localStorage.getItem('favorites')

        if (favorites) {
            this.setState({favorites: JSON.parse(favorites)})
        }

        this.setState({cards: data, isCardsLoading: false});

    }


    render() {
        const modalActive = this.state.modalActive
        const {isCardsLoading, cards, carts, isOpenModal, modalProps, favorites, addModalCards} = this.state;
        return (
            <div className={styles.App}>
                <Header/>
                <main>
                    <section className={styles.leftContainer}>
                        <h1>Каталог</h1>
                        {isCardsLoading && <Preloader size={90}/>}
                        {!isCardsLoading &&
                            <CardContainer addToFavorites={this.addToFavorites} handleClick={this.handleClick} addToCart={this.addToCart} cards={cards}/>}
                    </section>

                    <section className={styles.rightContainer}>
                        <h2>Корзина</h2>
                        <CartContainer setModalProps={this.setModalProps} toggleModal={this.toggleModal}
                                       incrementCartItem={this.incrementCartItem}
                                       decrementCartItem={this.decrementCartItem} carts={carts}/>

                    </section>
                </main>
                {modalActive &&
                    <AddModal
                        addToCart={this.addToCart}
                        closeButton={true}
                        clickHandler={this.handleClick}
                        addModalCards={addModalCards}
                    />

                }
                <Modal modalProps={modalProps} deleteCartItem={this.deleteCartItem} isOpen={isOpenModal}
                       toggleModal={this.toggleModal}/>
                {/*<FavoriteContainer favorites={favorites}/>*/}
            </div>
        );
    }
}

export default App;


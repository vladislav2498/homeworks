import React, {PureComponent} from 'react';
import CardItem from "../CardItem";
import styles from './CardContainer.module.scss';

class CardContainer extends PureComponent {
    render() {
        const { addToCart, cards, handleClick, addToFavorites} = this.props;
        return (
            <div>
                <ul className={styles.list}>
                    {cards.map(({name, url, color, price, code, isFavorite}) => (
                        <li key={code}>
                        <CardItem
                            addToFavorites={addToFavorites}
                            handleClick={handleClick}
                            addToCart={addToCart}
                            name={name}
                            url={url}
                            isFavorite={isFavorite}
                            color={color}
                            price={price}
                            code={code}/>
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}

export default CardContainer;
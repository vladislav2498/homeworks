import React, {PureComponent} from 'react';
import CartItem from "../CartItem";

class CartContainer extends PureComponent {
    render() {

        const { carts, incrementCartItem, decrementCartItem, toggleModal, setModalProps } =this.props

        return (
          <ul>
              <ul>
                  {carts.map(({name, url, count, id, code }) => {
                      return <CartItem
                          key={code}
                          name={name} url={url} count={count} id={id} code={code} incrementCartItem={incrementCartItem} decrementCartItem={decrementCartItem} toggleModal={toggleModal} setModalProps={setModalProps}/>
                  })}
              </ul>
          </ul>
        );
    }
}

export default CartContainer;
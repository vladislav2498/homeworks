import {PureComponent} from 'react';
import styles from './CardItem.module.scss';
import Button from '../Button';
import heartSvg from '../../svg/heart.svg'
import outlineHeartSvg from '../../svg/heart-outline.svg'

class CardItem extends PureComponent {

    state = {
        btnImage:  true
    }

    render() {

        const {btnImage} = this.state

        const { name, url, color, price, code, handleClick, addToFavorites} = this.props
        return (
            <div className={styles.card}>
                <button onClick={() => {
                    addToFavorites({name, url, code})
                    this.setState((prev) => ({
                        btnImage: !prev.btnImage
                    }))

                }} type="button" className={styles.likeButton}>
                    <img src={btnImage ? outlineHeartSvg : heartSvg} alt='favorite'/>
                </button>
                <span className={styles.title}>{name}</span>
                <img className={styles.itemAvatar} src={url}
                     alt={name}/>
                <span className={styles.description}>code: {code}</span>
                <span className={styles.description}>color: {color}</span>
                <span className={styles.description}>price: {price}$</span>

                <div className={styles.btnContainer}>
                    <Button onClick={() => {
                        handleClick(name, url, code)
                    }}>В корзину</Button>
                </div>
            </div>
        )
    }
}

export default CardItem;
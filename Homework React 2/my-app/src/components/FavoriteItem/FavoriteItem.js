import {PureComponent} from 'react';

class FavoriteItem extends PureComponent {
    render() {

        const {url, name, code} = this.props

        return (
            <div>
                <img src={url} alt={name}/>
            </div>
        );
    }
}

export default FavoriteItem;
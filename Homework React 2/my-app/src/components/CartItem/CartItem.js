import {PureComponent} from 'react';
import styles from './CartItem.module.scss';
import Button from '../Button'

class CartItem extends PureComponent {

    render() {

        const {name, url, count, incrementCartItem, decrementCartItem, code, toggleModal, setModalProps} = this.props

        return (
            <div className={styles.cartItem}>
                <div className={styles.contentContainer}>
                    <div className={styles.imgWrapper}>
                        <img className={styles.itemAvatar} src={url}
                             alt={name}/>
                    </div>
                </div>

                <span className={styles.quantity}>{count}</span>

                <div className={styles.btnContainer}>
                    <Button onClick={() => incrementCartItem(code)} className={styles.btn}>+</Button>
                    <Button onClick={() => decrementCartItem(code)} className={styles.btn}>-</Button>
                    <Button onClick={() => {
                        setModalProps({code, name})
                        toggleModal(true)}}
                            color='red' className={styles.btn}>DEL</Button>
                </div>

            </div>
        )
    }
}

export default CartItem;
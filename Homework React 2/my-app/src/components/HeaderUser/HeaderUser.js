import { PureComponent } from 'react';
import styles from './HeaderUser.module.scss';
import Preloader from "../Preloader";

class HeaderUser extends PureComponent {

    state = {
        userName: '',
        userImg: '',
        isLoading: true
    }

    async componentDidMount() {
        const {data: {name, avatar}} = await fetch('http://localhost:3001/user').then(res => res.json())
        this.setState({userName: name, userImg: avatar, isLoading: false})
    }

    render() {
        const {userName, userImg, isLoading} = this.state
        return (
            <div className={styles.container}>
                {isLoading && <Preloader color='#fff' size={30}/>}

                {!isLoading &&
                    <>
                    <img
                    src={userImg}
                    alt="John Smith"
                    width={30}
                    height={30}
                />
                <span>{userName}</span></>}
            </div>
        );
    }
                }

export default HeaderUser;
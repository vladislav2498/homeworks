const { user } = require('../../data');
const {sendGoodResponse} = require("../utils");

const getItems = (app) => {
    app.get('/user', (req, res) => {
        setTimeout(() => {
            sendGoodResponse(res, user);
        }, 3000)
    })
}
module.exports = getItems;
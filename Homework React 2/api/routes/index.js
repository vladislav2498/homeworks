const getUser = require('./getUser');
const getItems = require('./getItems');
const toggleIsFavorite = require('./toggleIsFavorite');
module.exports = (app) => {
    getItems(app);
    toggleIsFavorite(app);
    getUser(app)
}


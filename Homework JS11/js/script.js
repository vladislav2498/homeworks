"use strict";

const password = document.querySelector('#password')
const repeatPassword = document.querySelector('#repeat-password')
const btn = document.querySelector('.btn')


const checkPassword = () => {
    if (repeatPassword.value !== password.value || password.value === "") {
        document.querySelector('button').disabled = false;
        document.querySelector('.error').innerText = 'Passwords must be entered and match'
    } else {
        document.querySelector('.error').innerText = ''
        alert('You are welcome')
    }
}

btn.addEventListener('click', checkPassword)


const form = document.querySelector('.password-form');

form.addEventListener('click', (e) =>{
    let img = e.target.closest('img');
    if (!img) return;

    let input = img.parentNode.parentNode.querySelector('input')

    if (input.getAttribute('type') === 'password') {
        input.removeAttribute('type')
        input.setAttribute('type', 'text');
        img.src = 'svg/eye-off.svg'
    } else {
        input.removeAttribute('type');
        input.setAttribute('type', 'password');
        img.src = 'svg/eye.svg'
    }
})


"use strict";

/*  Теоретический вопрос 1

        Document Object Model — это независящий от платформы и языка программный интерфейс,
        позволяющий программам и скриптам получить доступ к содержимому HTML-, XHTML- и XML-документов, а также изменять содержимое, структуру
        и оформление таких документов.



    Теоретический вопрос 2

        innerText - показывает всё текстовое содержимое, которое не относится к синтаксису HTML. То есть любой текст, заключённый между
        открывающими и закрывающими тегами элемента будет записан в innerText. Причём если внутри innerText будут ещё какие-либо элементы
         HTML со своим содержимым, то он проигнорирует сами элементы и вернёт их внутренний текст.

innerHTML - покажет текстовую информацию ровно по одному элементу. В вывод попадёт и текст и разметка HTML-документа, которая может быть заключена между открывающими и закрывающими тегами основного элемента.



    Теоретический вопрос 3

        Есть 6 основных методов поиска элементов в DOM:

        querySelector
        querySelectorAll
        getElementById	id
        getElementsByName
        getElementsByTagName
        getElementsByClassName

        Безусловно, наиболее часто используемыми в настоящее время являются методы querySelector и querySelectorAll

 */



const getParagraph = document.getElementsByTagName('p');
for (let i=0; i<getParagraph.length; i++) {
    getParagraph[i].style.background = '#ff0000';
}

const getElementById = document.getElementById('optionsList')
console.log(getElementById);
const getParentElement = document.getElementById('optionsList').parentNode
console.log(getParentElement);
const getChildElement = document.getElementById('optionsList').childNodes
console.log(getChildElement);

const getElementForTest = document.getElementById('testParagraph').innerHTML = "<p>This is a paragraph</p>";

const getElementByClassMain = document.querySelector('.main-header').childNodes;
getElementByClassMain.forEach((elem) => elem.classList.add('nav-item'));

const getAllClasses = document.getElementsByClassName('section-title');
getAllClasses.classList.remove('section-title');




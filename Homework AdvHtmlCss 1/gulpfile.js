import gulp from "gulp";
import htmlmin from "gulp-htmlmin";
import concat from "gulp-concat";
import terser from 'gulp-terser';
import cleancss from 'gulp-clean-css';
import imagemin from "gulp-imagemin";
import browserSync from "browser-sync";
const bS = browserSync.create();

export const htmlMin = () => gulp.src("./src/*.html")
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./dest'))

export const css = () => gulp.src("./src/**/*.css")
    .pipe(concat('styles.min.css'))
    .pipe(cleancss())
    .pipe(gulp.dest('./dest/'));

export const js = () => gulp.src("./src/**/*.js")
    .pipe(concat('script.min.js'))
    .pipe(terser())
    .pipe(gulp.dest('./dest/'));

export const minImage = () => gulp.src("./src/images/**/*")
    .pipe(imagemin())
    .pipe(gulp.dest('./dest/img'))

export const build = gulp.parallel(css, js, htmlMin, minImage)

export const dev = () => {
    bS.init({
        server: {
            baseDir: "./dest/"
        }
    });
    gulp.watch('./src/**/*', gulp.series(build, (done) => {
        bS.reload();
        done();
    }));
}





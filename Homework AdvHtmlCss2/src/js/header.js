"use strict"


const btn = document.querySelector('.header__menu-button')

btn.addEventListener('click', () => {
    let img = document.querySelector('.header__menu-button img')
    if (document.querySelector(".header__navigation").classList.toggle("show")) {
        img.src = 'img/menu-button.png'
    } else {
        img.src = 'img/menu-button-lines.png'
    }
})
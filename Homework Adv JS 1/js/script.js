"use strict";

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name
    }

    set name(value) {
        return this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        return this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return super.salary * 3;
    }
}

const programmers = [
    new Programmer("Bob", 23, 2000, "ukr"),
    new Programmer("John", 32, 1800, "eng"),
    new Programmer("Danny", 21, 3200, "rus")
];
programmers.forEach(elem => {
    console.log(elem);
    console.log(`employee salary getter ${elem.salary}`);
});

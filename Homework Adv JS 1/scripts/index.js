/**
 * @return {boolean}
 */
const getRandomChance = () => Math.random() < 0.5;

class Animal {
    constructor(name) {
        if (name.length < 3) {
            this.name = "Anonimus"
        } else {
            this.name = name;
        }

        this.div = document.createElement(`div`)
        this.button = document.createElement('button');
        this.location = 0;
    }

    render(selector) {
        const container = document.querySelector(selector);
        this.div.className = 'animal';
        this.div.innerHTML = `<span>${this.name}</span>`
        this.button.innerText = 'Move';
        this.div.append(this.button);
        container.append(this.div)
        this.button.addEventListener('click', this.move.bind(this));
    }

    move() {
        this.location += 20;
        this.div.style.transform = `translateX(${this.location}px)`
    }

    say(selector) {
        this.div.classList.add(selector);
        setTimeout(() => {
            this.div.classList.remove(selector);
        }, 200);
    }
}

const bobby = new Animal('bob')

bobby.render('.container')

class Dog extends Animal {
    render(selector) {
        super.render(selector);
        this.div.classList.add('dog');
    }

    move() {
        super.move();
        if (getRandomChance()) {
            super.move();
            super.say('dog-say');
        }
    }
}


class Cat extends Animal {
    render(selector) {
        super.render(selector);
        this.div.classList.add('cat');
    }

    move() {
        super.move();
        if (getRandomChance()) {
            super.move();
        } else {
            this.say('cat-say');
        }
    }
}

class Snake extends Animal {
    constructor(name, type) {
        super(name);
        if (!(type !== "poison" && type !== "safe")) {
            this.type = "safe"
        } else {
            this.type = type
        }
    }
    render(selector) {
        super.render(selector);
        this.div.classList.add('snake');
    }
    move() {
        this.location -= 15
        super.move()
    }

}


const snake = new Snake("Pshh", "poison")
console.log(snake)
snake.render('.container');
const dog = new Dog('Dollar');
dog.render('.container');
const cat = new Cat("Musya")
cat.render('.container')



"use strict";

/*  Теоретический вопрос 1

        Экранирование символов - замена в тексте управляющих символов на соответствующие текстовые подстановкию.
        Допустим, мы хотим найти буквально точку. Не «любой символ», а именно точку.
        Чтобы использовать специальный символ как обычный, добавьте к нему обратную косую черту: \.
        Это называется «экранирование символа».



    Теоретический вопрос 2

        Существует три способа объявления функции: Function Declaration, Function Expression и Named Function Expression.



    Теоретический вопрос 3

        Поднятие или hoisting — это механизм в JavaScript, в котором переменные и объявления функций, передвигаются вверх
        своей области видимости перед тем, как код будет выполнен.
 */


const getUserInfo = (message) => {
    let userInfo;
    do {
        userInfo = prompt(message);
    }
    while (!userInfo)

    return userInfo;
}


const createNewUser = () => {
    let newUser = {
        firstName: getUserInfo("Enter your name"),
        lastName: getUserInfo("Enter your surname"),
        birthday: getUserInfo("Enter your date of birth in dd.mm.yyyy format"),
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },

        getAge() {
            let arrOfAllNumbers = this.birthday.split('.');
            const now = new Date();
            const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            const dob = new Date(+arrOfAllNumbers[2], arrOfAllNumbers[1] - 1, +arrOfAllNumbers[0]);
            const dobnow = new Date(today.getFullYear(), dob.getMonth(), dob.getDate());
            let age;
            age = today.getFullYear() - dob.getFullYear();
            if (today < dobnow) {
                age = age-1;
            }
            return age
        },

        getPassword() {
            let arrOfAllNumbers = this.birthday.split('.');
            const year = +arrOfAllNumbers[2]
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase() + year;
        }
    }

    Object.defineProperties(newUser, {
        'firstName': {
            writable: false,
        },
        'lastName': {
            writable: false,
        }
    })

    return newUser
}

console.log(createNewUser());
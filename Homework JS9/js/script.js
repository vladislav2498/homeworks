"use strict";

/*  Теоретический вопрос 1

        Это можно сделать несколькими способами:

        Метод document.createElement
        метод insertAdjacentHTML



    Теоретический вопрос 2

         Использование метода insertAdjacentHTML
         Это более универсальный метод, если вам необходимо вставить некий текст (изображение) внутрь другого тега
         или ДО тега, или ПОСЛЕ него. Для этого существуют специальные строки:

        "beforeBegin" - перед элементом.
        "afterBegin" - внутрь элемента, в самое начало, т.е. сразу за открывающим тегом.
        "beforeEnd" - внутрь элемента, в конец, т.е. перед закрывающим тегом.
        "afterEnd" - после элемента.



    Теоретический вопрос 3

        Удаление html-элементов методом removeChild()
        Удаление методом remove()


 */


const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

const getList = (arr, parentElem) => {
    const listHtml = arr.map((elem) => `<li>${elem}</li>`).join("");
    parentElem.insertAdjacentHTML("afterbegin", `<ul>${listHtml}</ul>`);
}


document.write(getList(arr, document.body))






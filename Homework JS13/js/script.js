"use strict";

const btnStart = document.querySelector('.btn-start')
const btnStop = document.querySelector('.btn-stop')
const image = document.querySelector('#image')

const images = [
    '../img/1.jpg',
    '../img/2.jpg',
    '../img/3.JPG',
    '../img/4.png',
]

let counter = 0;
let timer;


let interval = () => {
   return setInterval(() => {
       pictureCounter()
   }, 1000)
}

const pictureCounter = () => {
    if (counter === images.length - 1) {
        counter = 0;
    } else {
        counter++
    }
    image.src = images[counter];
}

btnStop.addEventListener('click', () => {
    clearInterval(timer);
})

btnStart.addEventListener('click', () => {
    timer = interval();
})
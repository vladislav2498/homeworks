"use strict"

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

class NotValuesError extends Error {
    constructor(value) {
        super();
        this.name = "BookError"
        this.message = `Property: ${value} is not defined`
    }
}

class Book {
    constructor(book) {
        if (book['price'] === undefined) {
            throw new NotValuesError('price');
        }
        if (book['author'] === undefined) {
            throw new NotValuesError('author');
        }
        if (book['name'] === undefined) {
            throw new NotValuesError('name');
        }
        this.book = book;
    }

    render(container) {
        container.insertAdjacentHTML('beforeend', `<ul><li>${this.book.author}, ${this.book.name}, ${this.book.price}</li></ul>`)
    }
}

const container = document.querySelector('#root')

books.forEach((el) => {
    try {
        new Book(el).render(container)
    } catch (err)  {
        if (err.name === "BookError") {
            console.warn(err);
        } else {
            throw err;
        }
    }
})
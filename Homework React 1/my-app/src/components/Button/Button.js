import React from 'react'
import styles from './Button.module.scss'


class Button extends React.PureComponent {
    render() {

        const {backgroundColor, text, onClick} = this.props

        return (
            <button className={styles.btn} style={{backgroundColor}} onClick={onClick} type='button'>{text}</button>
        )
    }
}

export default Button
import React, {Component} from 'react';
import Button from './components/Button/Button';
import Modal from "./components/Modal/Modal";
import styles from './App.module.scss';

class App extends Component {

    state = {
        modalActive: false,
        modalActiveSecond: false,
    }

    handleClick = () => {
        this.setState((prev) => ({modalActive: !prev.modalActive}))
    }

    handleClickSecond = () => {
        this.setState((prev) => ({modalActiveSecond: !prev.modalActiveSecond}))
    }

    render() {

        const modalActive = this.state.modalActive
        const modalActiveSecond = this.state.modalActiveSecond

        return (
            <div>
                <div className={styles.button__container}>
                    <Button backgroundColor='blue' text='Open first modal' onClick={this.handleClick
                    }/>
                    <Button backgroundColor='yellow' text='Open second modal' onClick={this.handleClickSecond}/>
                </div>
                {modalActive &&
                    <Modal header='Do you want to delete this file?'
                           closeButton={true}
                           text='Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?'
                           actions={
                               <div className={styles.buttons}>
                                   <button onClick={this.handleClick}>Ok</button>
                                   <button onClick={this.handleClick}>Cancel</button>
                               </div>
                           } clickHandler={this.handleClick}/>
                }
                {modalActiveSecond &&
                    <Modal header='Why so serious?'
                           backgroundColor="green"
                           closeButton={true}
                           text='Do you want to buy a batmobile?'
                           actions={
                               <div className={styles.buttons}>
                                   <button onClick={this.handleClickSecond}>Ok</button>
                                   <button onClick={this.handleClickSecond}>Cancel</button>
                               </div>
                           } clickHandler={this.handleClickSecond}/>}
            </div>
        )
    }
}


export default App

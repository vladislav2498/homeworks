import {Routes, Route} from 'react-router-dom'
import Showcase from "./pages/Showcase/Showcase";
import Cart from "./pages/Cart/Cart";
import Favorite from "./pages/Favorite/Favorite";

const AppRoutes = () => {
    return (
        <Routes>
            <Route path='/' element={<Showcase/>}/>
            <Route path='/favorite' element={<Favorite/>}/>
            <Route path='/cart' element={<Cart/>}/>
        </Routes>
    )
}

export default AppRoutes
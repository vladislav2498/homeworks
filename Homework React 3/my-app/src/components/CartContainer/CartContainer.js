import React from 'react';
import CartItem from "../CartItem";

const CartContainer = ({carts, incrementCartItem, decrementCartItem, toggleModal, setModalProps}) => {

    return (
        <ul>
            <ul>
                {carts.map(({name, url, count, id, code}) => {
                    return <CartItem
                        key={code}
                        name={name} url={url} count={count} id={id} code={code} incrementCartItem={incrementCartItem}
                        decrementCartItem={decrementCartItem} toggleModal={toggleModal} setModalProps={setModalProps}/>
                })}
            </ul>
        </ul>
    );
}

export default CartContainer;

import React, {useState, useEffect} from 'react';
import styles from "../../App.module.scss";
import CartContainer from "../../components/CartContainer/CartContainer";
import Modal from "../../components/Modal";

const Cart = () => {

    const [carts, setCarts] = useState([])
    const [isOpenModal, setIsOpenModal] = useState(false)
    const [modalProps, setModalProps] = useState({})


    const deleteCartItem = (code) => {
        setCarts((current) => {
            const carts = [...current]

            const index = carts.findIndex(el => el.code === code)


            if (index !== -1) {
                carts.splice(index, 1);
            }

            localStorage.setItem('carts', JSON.stringify(carts))

            return carts
        })
    }

    const incrementCartItem = (code) => {
        setCarts((current) => {
            const carts = [...current]

            const index = carts.findIndex(el => el.code === code)

            if (index !== -1) {
                carts[index].count += 1
            }

            localStorage.setItem('carts', JSON.stringify(carts))

            return carts
        })
    }

    const decrementCartItem = (code) => {
        setCarts((current) => {
            const carts = [...current]

            const index = carts.findIndex(el => el.code === code)

            if (index !== -1 && carts[index].count > 1) {
                carts[index].count -= 1
            }

            localStorage.setItem('carts', JSON.stringify(carts))

            return carts
        })
    }

    const toggleModal = (value) => {
        setIsOpenModal(value)
    }

    useEffect(() => {

        const carts = localStorage.getItem('carts')

        if (carts) {
            setCarts(JSON.parse(carts))
        }
    }, [])



    return (
        <section className={styles.rightContainer}>
            <h2>Cart</h2>
            <CartContainer setModalProps={setModalProps} toggleModal={toggleModal}
                           incrementCartItem={incrementCartItem}
                           decrementCartItem={decrementCartItem} carts={carts}/>

            <Modal modalProps={modalProps} deleteCartItem={deleteCartItem} isOpen={isOpenModal}
                   toggleModal={toggleModal}/>
        </section>
    );
};

export default Cart;
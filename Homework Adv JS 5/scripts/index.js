"use strict"


class Card {
    constructor(username, name, email, title, body, id) {
        this.username = username;
        this.name = name;
        this.email = email;
        this.title = title;
        this.body = body;
        this.id = id;
        this.container = document.querySelector('.container')
        this.card = document.createElement('div');
        this.card.className = 'card'
        this.card.id = `${this.id}`
        this.deleteButton = document.createElement('button')
        this.deleteButton.id = `${this.id}`
    }

    createElements() {
        this.deleteButton.innerHTML = "Delete"

        this.card.insertAdjacentHTML('beforeend', `
          
           <h2 style="text-align:center">${this.username}</h2>
           <div class="card">
  <h1>${this.name}</h1>
  <p class="title">${this.title}</p>
  <p>${this.body}</p>
  <div style="margin-top: 24px;">
    <a href="#"><i class="fa fa-dribbble">${this.email}</i></a> 
 </div>
</div>
        `)
        this.card.append(this.deleteButton);
        this.container.append(this.card);

        this.deleteButton.addEventListener('click', () => {

            fetch(`https://ajax.test-danit.com/api/json/users/1`, {
                method: 'DELETE'
            })
                .then(response => {
                    if(response.status === 200 && this.card.id === this.deleteButton.id) {
                        this.card.remove()
                    }
                })

        })
    }

    render() {
        this.createElements()
    }
}

Promise.all([
    fetch("https://ajax.test-danit.com/api/json/users").then((res) =>
        res.json().then((data) => data)
    ),
    fetch("https://ajax.test-danit.com/api/json/posts").then((res) =>
        res.json().then((data) => data)
    )
]).then((data) => {
    const[userArr, postsArr] = data;
    let cardArr = document.querySelector(".container");
    postsArr.forEach(({title, body, id, userId}) => {
        const user = userArr.find((item) => item.id === userId);
        new Card(
            user.username,
            user.name,
            user.email,
            title,
            body,
            id,
            userId,
        ).render(cardArr)
    })
})

.catch(()=> {
    alert('Something went wrong! Please, try to reload the page')
})
"use strict";

/*  Теоретический вопрос 1

         Функции является основными строительными элементами программы. Функции нужны для того что бы не повторять один
         и тот же код во многих местах. Чем проще функция тем лучше. Идеально когда одна функция выполняет одно просто действие.



    Теоретический вопрос 2

        Аргумент передается для дальнейшего использования в функции. Мы можем передать внутрь функции любую информацию, используя
        аргументы. Аргумент – это значение, которое передаётся функции при её вызове. Мы объявляем функции со списком параметров,
        затем вызываем их, передавая аргументы.



    Теоретический вопрос 3

        Оператор return завершает выполнение функции и возвращает управление вызывающей функции. Выполнение возобновляется в
        вызывающей функции в точке сразу после вызова. Оператор return может возвращать значение, передавая его вызывающей
        функции.
 */

const getNumber = (message) => {
    let userNumber;

    do {
        userNumber = prompt(message, userNumber)
    }
    while (isNaN(+userNumber) || userNumber === "" || userNumber === null)
    return userNumber;
}

const getSign = (message) => {
    let userSign;

    do {
        userSign = prompt("Enter the sign: '+' '-' '*' '/' ", userSign);
    }
    while (userSign !== '+' && userSign !== '-' && userSign !== '*' && userSign !== '/');
    return userSign;
}

const addition = () => {
    const firstNumber = getNumber("Enter first number");
    const secondNumber = getNumber("Enter second number");
    return +firstNumber + +secondNumber;
}

const subtraction = () => {
    const firstNumber = getNumber("Enter first number");
    const secondNumber = getNumber("Enter second number");
    return firstNumber - secondNumber;
}

const multiplication = () => {
    const firstNumber = getNumber("Enter first number");
    const secondNumber = getNumber("Enter second number");
    return firstNumber * secondNumber;
}

const division = () => {
    const firstNumber = getNumber("Enter first number");
    const secondNumber = getNumber("Enter second number");
    return firstNumber / secondNumber;
}

const calculator = () => {
    const userChoice = getSign();

    switch (userChoice) {
        case "+" :
            return addition();
        case "-" :
            return subtraction();
        case "*" :
            return multiplication();
        case "/" :
            return division();
    }
}

console.log(calculator());
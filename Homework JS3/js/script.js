"use strict";

/*  Теоретический вопрос 1

         Циклы в Javascript нужны для того чтобы сделать однотипное действие много раз, например, вывести товары из списка один за другим или просто
         перебрать все числа в заданном диапазоне и для каждого выполнить одинаковый код или для многократного повторения одного участка кода.



    Теоретический вопрос 2

        В предидущем домашнем задании использовался бесконечный цикл while для того чтобы добиться от пользователя корректного ввода данных в поле ввода.

         let userName = prompt("What is your name?");
            while (userName === '' || userName === ' ') {
            userName = prompt("What is your name?");
            }

        let userAge = +prompt("How old are you?");
            while (isNaN(userAge) || userAge === '' || userAge === ' ' || userAge === 0) {
            userAge = +prompt("How old are you?");
            }

        Также в практических заданиях использовался цикл for для того чтобы перечислить диапазон чисел с определенными свойствами
        (например кратность какому-либо числу).



    Теоретический вопрос 3

        Преобразование типов может быть явным и неявным. Когда разработчик выражает намерение сконвертировать значение одного типа
        в значение другого типа, записывая это соответствующим образом в коде, скажем, в виде Number(value), это называется явным приведением
        типов (или явным преобразованием типов).
        Для явного преобразования в примитивные типы данных в JavaScript используются следующие функции: Boolean(), Number(), String(). При неявном
        преобразования интерпретатор JavaScript автоматически использует те же самые функции, что используются и для явного преобразования.
 */



"use strict"

let myNumber = +prompt("Enter the number");

if (!Number.isInteger(myNumber)) {
    alert("Sorry! Enter the number");

    while (!Number.isInteger(myNumber)) {
        myNumber = +prompt("Enter the number");
    }
}

if (myNumber < 5) {
    console.log("Sorry, no numbers")
} else {
    for (let i = 1; i <= myNumber; i++) {
        if (i % 5 === 0) {
            console.log(i)
        } else if (myNumber < 5) {
            console.log("Sorry, no numbers")
        }
    }
}





